/*
 * SPDX-FileCopyrightText: 2010-2022 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: CC0-1.0
 */

#include "sdk.h"
#include "afw.h"
#include "logger.h"
#include "driver/gpio.h"
#include "esp_wifi.h"

#define LED_IO      GPIO_NUM_2
#define BUTTON_IO   GPIO_NUM_0

const char* definition = "{\"model\":\"PLWF\",\"firmware\":100,\"hardware\":101,\"switch_1\":false,\"timer_1\":0,\"cur_vol\":0,\"cur_cur\":0,\"cur_pwr\":0,\"schedules\":[{\"code\":\"schedule_1\",\"enable\":true,\"time\":\"1200\",\"repeat\":127,\"notify\":true,\"target\":{\"switch_1\":true}}],\"settings\":{\"manual_mode\":true,\"cont_mode\":false}}";

afdata switch_1 = null;
afdata timer_1 = null;
afdata voltage = null;
afdata current = null;
afdata power = null;

static QueueHandle_t gpio_evt_queue = NULL;

static bool on_property_change(void* property, int value, int cause) {
    if (property == switch_1)
        gpio_set_level(LED_IO, value);
    else if (property == timer_1) {
        if (value == 0) {
            afw_set_property_int(switch_1, afw_get_property_int(switch_1) == 0);
            gpio_set_level(LED_IO, afw_get_property_int(switch_1));
        }
    }
    return true;
}

static bool on_parameter_change(void* parameter, int value, int cause) {
    logi("param change -> %d", value);
    // if (parameter == thres_temp)
    //     afw_set_property_int_local(temperature_set, value);
    return true;
}

static void on_schedule_tick(int hour, int min, const char* target) {
    logi("schedule tick: (%d:%d) %s", hour, min, target);
}

static void IRAM_ATTR gpio_isr_handler(void* arg) {
    uint32_t gpio_num = (uint32_t) arg;
    xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

static void app_reset_wifi_proc(void) {
    if (gpio_get_level(BUTTON_IO) == 0)
        afw_reset_wifi();
}

static void gpio_task_example(void* arg) {
    uint32_t io_num;
    while (true)
    {
        if(xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
            if (io_num == BUTTON_IO)
                start_timer(5000, app_reset_wifi_proc);
            start_timer(5000, app_reset_wifi_proc);
            logi("GPIO[%"PRIu32"] intr", io_num);
        }
    }
}

int led_blink_period = 1000;
static void blink_led(void) {
    static bool state = false;
    state = !state;
    gpio_set_level(LED_IO, state);
    start_timer(led_blink_period, blink_led);
}

// static void test_switch(void) {
//     static bool state = false;
//     state = !state;
//     afw_set_property_int(switch_1, true);
//     start_timer(5000, test_switch);
// }

static void measure_temperature_proc(void) {
    static int temp = 0;
    start_timer(30000, measure_temperature_proc);
    temp = 219 + (int)(xTaskGetTickCount() & 0x07);
    if (temp != afw_get_property_int(voltage)) {
        afw_set_property_int(voltage, temp);
        logi("voltage: %d (V)", temp);
    }

    temp = 0 + (int)(xTaskGetTickCount() & 0x07);
    if (temp != afw_get_property_int(current)) {
        afw_set_property_int(current, temp);
        logi("current: %d (mA)", temp);
    }

    temp = 100 + (int)(xTaskGetTickCount() & 0x07);
    if (temp != afw_get_property_int(power)) {
        afw_set_property_int(power, temp);
        logi("power: %d (W)", temp);
    }
}

static void on_nwk_status_proc(int status) {
    logi("Nwk status -> %d", status);
    if (status == NWK_IF_CONNECTED) {
        cancel_timer(blink_led);
        gpio_set_level(LED_IO, true);
        afw_set_property_int(switch_1, true);
    }
    else if (status == NWK_SMARTCONFIG_READY) {
        led_blink_period = 100;
        start_timer(led_blink_period, blink_led);
    }
    else if (status == NWK_IF_DISCONNECTED) {
        led_blink_period = 1000;
        start_timer(led_blink_period, blink_led);
    }
}

static void app_io_init(void) {
    gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
    xTaskCreate(gpio_task_example, "gpio_task_example", 2048, NULL, 10, NULL);

    gpio_config_t cfg;
    cfg.pin_bit_mask    = 1ULL << LED_IO;
    cfg.mode            = GPIO_MODE_OUTPUT;
    cfg.pull_up_en      = GPIO_PULLUP_DISABLE;
    cfg.pull_down_en    = GPIO_PULLDOWN_DISABLE;
    gpio_config(&cfg);
    gpio_set_level(LED_IO, 0);

    cfg.intr_type = GPIO_INTR_NEGEDGE;
    cfg.pin_bit_mask = 1 << BUTTON_IO;
    cfg.mode = GPIO_MODE_INPUT;
    gpio_config(&cfg);
    gpio_install_isr_service(0);
    gpio_isr_handler_add(BUTTON_IO, gpio_isr_handler, (void*)BUTTON_IO);
}

void app_main(void)
{
    app_io_init();

    afw_register_network_callback(on_nwk_status_proc);
    afw_register_property_callback(on_property_change);
    afw_register_parameter_callback(on_parameter_change);
    afw_register_schedule_tick_callback(on_schedule_tick);
    afw_init(definition, 0);

    switch_1 = afw_get_property("switch_1");
    timer_1 = afw_get_property("timer_1");
    voltage = afw_get_property("cur_vol");
    current = afw_get_property("cur_cur");
    power = afw_get_property("cur_pwr");
    
    // thres_temp = afw_get_parameter("thres_temp");

    start_timer(led_blink_period, blink_led);
    start_timer(1000, measure_temperature_proc);
    // start_timer(5000, test_switch);

    uint32_t free_heap = 0;
    uint32_t current_heap = 0;
    while(true)
    {
        current_heap = esp_get_free_heap_size();
        if (free_heap != current_heap) {
            free_heap = current_heap;
            logi("Memory free %"PRIu32", min free %"PRIu32"", free_heap, esp_get_minimum_free_heap_size());
        }
        vTaskDelay(pdMS_TO_TICKS(2000));
    }
}
